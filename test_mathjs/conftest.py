import pytest


def pytest_addoption(parser):

    parser.addoption("--eps", metavar="eps",
                     default=0.001, )


@pytest.fixture
def eps(request):
    try:
        return float(request.config.getoption("--eps"))
    except ValueError:
        pytest.exit("Unacceptable eps parameter")
