from constants import (
    API_URL, HEADER,
    LEGAL_VALUES,
    VULNERABLE_VALUES,
    ILLEGAL_VALUES,
    MEASUREMENT_UNITS,
)
import requests
import pytest

SQRT_LEGAL_VALUES = LEGAL_VALUES[:]
SQRT_LEGAL_VALUES.append(0)


def sqrt(value):
    try:
        return value ** 0.5
    except TypeError:
        pytest.exit("Not a numeric value")


@pytest.mark.test_sqrt
@pytest.mark.parametrize("x", filter(lambda x: x >= 0, SQRT_LEGAL_VALUES),
                         ids=lambda x: f" Square root of positive argument: {x}")
def test_sqrt_positive(eps, x):
    response = requests.get(f"{API_URL}?expr=sqrt({x})", headers=HEADER)
    answer = response.json()
    assert pytest.approx(sqrt(x), eps) == pytest.approx(answer, eps)


@pytest.mark.test_sqrt
@pytest.mark.parametrize("x", filter(lambda x: x < 0, SQRT_LEGAL_VALUES),
                         ids=lambda x: f" Square root of negative argument:{x}")
def test_sqrt_negative(eps, x):
    response = requests.get(f"{API_URL}?expr=sqrt({x})", headers=HEADER)
    answer_str = response.text.replace("i", "j")
    imaginary = complex(answer_str).imag
    assert imaginary == pytest.approx(sqrt(-x), eps)


@pytest.mark.test_sqrt
@pytest.mark.parametrize("x", ILLEGAL_VALUES,
                         ids=lambda x: f" Square root of illegal argument:{x}")
def test_sqrt_illegal(x):
    response = requests.get(f"{API_URL}?expr=sqrt({x})", headers=HEADER)
    assert response.status_code == 400
    assert "error" in response.text.lower()


@pytest.mark.test_sqrt
@pytest.mark.parametrize("x", filter(lambda x: not (float(x) < 0), VULNERABLE_VALUES.keys()),
                         ids=lambda x: f" Square root of illegal argument:{x}")
def test_sqrt_vulnerable(x):
    response = requests.get(f"{API_URL}?expr=sqrt({x})", headers=HEADER)
    assert response.text == x


@pytest.mark.test_sqrt
@pytest.mark.parametrize("x", MEASUREMENT_UNITS,
                         ids=lambda x: f" Measure unit:{x}")
def test_sqrt_measuremet_units(x):
    response = requests.get(f"{API_URL}?expr=sqrt({x})", headers=HEADER)
    assert response.text == f"{x}^0.5"
