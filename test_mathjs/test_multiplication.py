from json import dumps
from constants import (
    API_URL, HEADER, LEGAL_VALUES,
    ILLEGAL_VALUES, VULNERABLE_VALUES,
    ALL_POSSIBLE
)
import requests
import pytest


MULT_LEGAL_VALUES = LEGAL_VALUES[:]
MULT_LEGAL_VALUES.append(0)
MULT_ALL_POSSIBLE = ALL_POSSIBLE[:]
MULT_ALL_POSSIBLE.append(0)


@pytest.mark.test_mult
@pytest.mark.parametrize("x", MULT_LEGAL_VALUES,
                         ids=lambda x: f"Factor 1: {x}")
@pytest.mark.parametrize("y", MULT_LEGAL_VALUES,
                         ids=lambda x: f"Factor 2: {x}")
def test_multiplication(eps, x, y):
    json_data = {"expr": f"{x}*{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = float(response.json().get("result"))
    assert pytest.approx(result, eps) == float(x) * float(y)


@pytest.mark.test_mult
@pytest.mark.parametrize("x", ILLEGAL_VALUES + MULT_LEGAL_VALUES,
                         ids=lambda x: f"Factor 1: {x}")
@pytest.mark.parametrize("y", ILLEGAL_VALUES,
                         ids=lambda x: f"Illegal factor 2: {x}")
def test_multiplication_illegal(x, y):
    json_data = {"expr": f"{x}*{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    error = response.json().get("error")
    assert error is not None


@pytest.mark.test_mult
@pytest.mark.parametrize("x", VULNERABLE_VALUES.keys(),
                         ids=lambda x: f"Factor 1: {x}")
@pytest.mark.parametrize("y", MULT_ALL_POSSIBLE,
                         ids=lambda x: f"Factor 2: {x}")
def test_multiplication_vulnerable(x, y):
    json_data = {"expr": f"{x}*{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = response.json().get("result")
    assert result in VULNERABLE_VALUES
