from json import dumps
from constants import (
    API_URL, HEADER, LEGAL_VALUES,
    ILLEGAL_VALUES, VULNERABLE_VALUES,
    ALL_POSSIBLE
)
import requests
import pytest


SUB_LEGAL_VALUES = LEGAL_VALUES[:]
SUB_LEGAL_VALUES.append(0)
SUB_ALL_POSSIBLE = ALL_POSSIBLE[:]
SUB_ALL_POSSIBLE.append(0)


@pytest.mark.test_sub
@pytest.mark.parametrize("x", SUB_LEGAL_VALUES,
                         ids=lambda x: f"Minuend: {x}")
@pytest.mark.parametrize("y", SUB_LEGAL_VALUES,
                         ids=lambda x: f"Subtrahend: {x}")
def test_subtraction(eps, x, y):
    json_data = {"expr": f"{x}-{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = float(response.json().get("result"))
    assert pytest.approx(result, eps) == float(x) - float(y)


@pytest.mark.test_sub
@pytest.mark.parametrize("x", SUB_LEGAL_VALUES + ILLEGAL_VALUES,
                         ids=lambda x: f"Minuend: {x}")
@pytest.mark.parametrize("y", ILLEGAL_VALUES,
                         ids=lambda x: f"Subtrahend: {x}")
def test_subtraction_illegal_minuend(x, y):
    json_data = {"expr": f"{x}-{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    error = response.json().get("error")
    assert error is not None


@pytest.mark.test_sub
@pytest.mark.parametrize("x", filter(lambda x: x, ILLEGAL_VALUES),  # avoiding -x expression
                         ids=lambda x: f"Minuend: {x}")
@pytest.mark.parametrize("y", SUB_LEGAL_VALUES + ILLEGAL_VALUES,
                         ids=lambda x: f"Subtrahend: {x}")
def test_subtraction_illegal_subtrahend(x, y):
    json_data = {"expr": f"{x}-{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    error = response.json().get("error")
    assert error is not None


@pytest.mark.test_sub
@pytest.mark.parametrize("x", VULNERABLE_VALUES.keys(),
                         ids=lambda x: f"Vulnerable minuend: {x}")
@pytest.mark.parametrize("y", SUB_ALL_POSSIBLE,
                         ids=lambda x: f"Vulnerable subtrahend: {x}")
def test_vulnerable(x, y):
    json_data = {"expr": f"{x}-{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = response.json().get("result")
    assert result in VULNERABLE_VALUES
