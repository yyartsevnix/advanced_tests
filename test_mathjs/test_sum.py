from json import dumps
from constants import (
    API_URL, HEADER, LEGAL_VALUES,
    ILLEGAL_VALUES, VULNERABLE_VALUES,
    ALL_POSSIBLE
)
import requests
import pytest

SUM_LEGAL_VALUES = LEGAL_VALUES[:]
SUM_ALL_POSSIBLE = ALL_POSSIBLE[:]
SUM_LEGAL_VALUES.append(0)
SUM_ALL_POSSIBLE.append(0)


@pytest.mark.test_sum
@pytest.mark.parametrize("x", SUM_LEGAL_VALUES,
                         ids=lambda x: f"Addend 1: {x}")
@pytest.mark.parametrize("y", SUM_LEGAL_VALUES,
                         ids=lambda x: f"Addend 2: {x}")
def test_sum(eps, x, y):
    json_data = {"expr": f"{x}+{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = float(response.json().get("result"))
    assert pytest.approx(result, eps) == float(x) + float(y)


@pytest.mark.test_sum
@pytest.mark.parametrize("x", SUM_LEGAL_VALUES,
                         ids=lambda x: f"Addend 1(Legal): {x}")
@pytest.mark.parametrize("y", ILLEGAL_VALUES,
                         ids=lambda x: f"Addend 2(Illegal): {x}")
def test_sum_illegal(x, y):
    json_data = {"expr": f"{x}+{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    resp_json = response.json()
    assert resp_json.get("error") is not None


@pytest.mark.test_sum
@pytest.mark.parametrize("x", VULNERABLE_VALUES.keys(),
                         ids=lambda x: f"Vulnerable addend 1: {x}")
@pytest.mark.parametrize("y", SUM_ALL_POSSIBLE,
                         ids=lambda x: f"not vulnerable addend 2: {x}")
def test_sum_vulnerable(x, y):
    json_data = {"expr": f"{x}+{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = response.json().get("result")
    assert result in VULNERABLE_VALUES
