from json import dumps
from copy import deepcopy
from constants import (
    API_URL, HEADER, LEGAL_VALUES,
    ILLEGAL_VALUES, VULNERABLE_VALUES,
    ALL_POSSIBLE
)
import requests
import pytest

DIV_VULNERABLE_VALUES = deepcopy(VULNERABLE_VALUES)
DIV_VULNERABLE_VALUES.update({'0': 0})


@pytest.mark.test_division
@pytest.mark.parametrize("x", LEGAL_VALUES,
                         ids=lambda x: f"Dividend: {x}")
@pytest.mark.parametrize("y", LEGAL_VALUES,
                         ids=lambda x: f"Divider: {x}")
def test_division(eps, x, y):
    json_data = {"expr": f"{x}/{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = float(response.json().get("result"))
    assert pytest.approx(result, eps) == float(x) / float(y)


@pytest.mark.test_division
@pytest.mark.parametrize("x", ILLEGAL_VALUES,
                         ids=lambda x: f"Illegal Dividend: {x}")
@pytest.mark.parametrize("y", ALL_POSSIBLE,
                         ids=lambda x: f"Divider: {x}")
def test_division_illegal_dividend(x, y):
    json_data = {"expr": f"{x}/{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    error = response.json().get("error")
    assert error is not None


@pytest.mark.test_division
@pytest.mark.parametrize("x", ALL_POSSIBLE,
                         ids=lambda x: f"Dividend: {x}")
@pytest.mark.parametrize("y", ILLEGAL_VALUES,
                         ids=lambda x: f"Illegal divider: {x}")
def test_division_illegal_divider(x, y):
    json_data = {"expr": f"{x}/{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    error = response.json().get("error")
    assert error is not None


@pytest.mark.vuln
@pytest.mark.test_division
@pytest.mark.parametrize("x", DIV_VULNERABLE_VALUES.keys(),
                         ids=lambda x: f"Vulnerable dividend: {x}")
@pytest.mark.parametrize("y", ALL_POSSIBLE, ids=lambda x: f"Divider: {x}")
def test_division_vulnerable_dividend(x, y):
    json_data = {"expr": f"{x}/{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = response.json().get("result")
    assert result in DIV_VULNERABLE_VALUES


@pytest.mark.vuln
@pytest.mark.test_division
@pytest.mark.parametrize("x", ALL_POSSIBLE,
                         ids=lambda x: f"Dividend: {x}")
@pytest.mark.parametrize("y", DIV_VULNERABLE_VALUES.keys(), ids=lambda x: f"Vulnerable divider: {x}")
def test_division_vulnerable_divider(x, y):
    json_data = {"expr": f"{x}/{y}"}
    response = requests.post(API_URL, headers=HEADER, data=dumps(json_data))
    result = response.json().get("result")
    assert result in DIV_VULNERABLE_VALUES
