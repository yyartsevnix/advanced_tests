CUSTNAME_LEGAL = ["Yegor", "@! $%"]
CUSTTEL_LEGAL = ["937-99-92", ]
CUSTEMAIL_LEGAL = ["mailbox@ex.com", ]
SIZES = ["small", "medium", "large"]
DELIVERY_LEGAL = ["11:00 AM", "12:30 PM", ]
TOPPINGS = ("bacon", "cheese", "onion", "mushroom")
post_form_url = "https://httpbin.org/forms/post"

ILLEGAL_EMAIL = ["aa@dd@bb", "23456"]
ILLEGAL_DELIVERY = ["10:00 AM", "10:15 PM", "11:40 AM"]

flmdb_post_form_url = "http://127.0.0.1:5000/post_form"
