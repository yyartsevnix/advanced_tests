from page_objects import PageObject, PageElement
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import re


class PizzaPage(PageObject):
    name = PageElement(name="custname")
    telephone = PageElement(name="custtel")
    __delivery = PageElement(name="delivery")
    email = PageElement(name="custemail")
    instructions = PageElement(name="comments")

    def set_size(self, value: str):
        if not isinstance(value, str):
            raise ValueError("Value must be a string")
        try:
            radio_size = self.w.find_element_by_xpath(f"//input[@name=\"size\"][@value=\"{value}\"]")
        except NoSuchElementException:
            raise ValueError(f"Radiobutton with value \"{value}\" not found")
        radio_size.click()

    def set_topping(self, *args):

        for i in args:
            if not isinstance(i, str):
                raise ValueError("Only string values are accepted")
            try:
                i = i.lower()
                topping_checkbox = self.w.find_element_by_xpath(f"//input[@name=\"topping\"][@value=\"{i}\"]")
                if not topping_checkbox.is_selected():
                    topping_checkbox.click()
            except NoSuchElementException:
                raise ValueError(f"No checkbox button with value {i} found")

    def unset_topping(self, *args):
        for i in args:
            if not isinstance(i, str):
                raise ValueError("Only string values are accepted")
            try:
                i = i.lower()
                topping_checkbox = self.w.find_element_by_xpath(f"//input[@name=\"topping\"][@value=\"{i}\"]")
                if topping_checkbox.is_selected():
                    topping_checkbox.click()
            except NoSuchElementException:
                raise ValueError(f"No checkbox button with value {i} found")

    def set_delivery(self, value):
        match = re.search(r"(AM|PM)", value)  # 12:45 AM, 01:00 PM
        if not match:
            raise ValueError("Invalid input for time form")
        time_list = value.split()
        time_input = self.w.find_element_by_xpath("//input[@type=\"time\"]")
        self.__delivery = time_list[0]
        if time_list[1] == "AM":
            time_input.send_keys(Keys.ARROW_UP)
        else:
            time_input.send_keys(Keys.ARROW_DOWN)

    @property
    def delivery(self):
        return self.__delivery

    def submit(self):
        button = self.w.find_element_by_xpath("//button")
        button.click()
