from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
import pytest


@pytest.fixture(scope="function",)
def driver():
    driver_options = Options()
    driver_options.add_argument("--headless")
    _driver = webdriver.Chrome(ChromeDriverManager().install(), options=driver_options)
    yield _driver
    _driver.close()
