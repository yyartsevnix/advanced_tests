from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from test_selenium.flmdb_get_post_form import GetFormPage, PostFormPage
from test_selenium import constants
from test_selenium.teststools import wait_for_form
import hamcrest
import pytest
from time import sleep


def test_post_form_empty(driver):
    driver.get(constants.flmdb_post_form_url)
    wait_for_form(driver)
    page = PostFormPage(driver)
    page.submit()
    page_content = driver.find_element_by_xpath('//').text
    assert page_content
