from json import loads

import hamcrest
import pytest

from test_selenium.constants import (
    CUSTEMAIL_LEGAL, ILLEGAL_DELIVERY,
    CUSTNAME_LEGAL, CUSTTEL_LEGAL,
    SIZES, DELIVERY_LEGAL,
    TOPPINGS, ILLEGAL_EMAIL,
)
from test_selenium.teststools import (
    am_pm_to_24, set_ids, wait_for_form
)
from test_selenium.order_page import PizzaPage
from test_selenium.constants import post_form_url


@pytest.mark.empty_form
def test_empty(driver):
    check_data = {
        "comments": "",
        "custemail": "",
        "custname": "",
        "custtel": "",
        "delivery": ""
    }
    driver.get(post_form_url)
    wait_for_form(driver)
    page = PizzaPage(driver)
    page.submit()
    html_body = driver.find_element_by_xpath("//body")
    response_dict = loads(html_body.text)
    form_data = response_dict.get("form")
    hamcrest.assert_that(form_data, hamcrest.has_entries(check_data))


@pytest.mark.one_topping
@pytest.mark.parametrize("name", CUSTNAME_LEGAL, ids=set_ids("Name"))
@pytest.mark.parametrize("telephone", CUSTTEL_LEGAL, ids=set_ids("telephone"))
@pytest.mark.parametrize("email", CUSTEMAIL_LEGAL, ids=set_ids("email"))
@pytest.mark.parametrize("delivery", DELIVERY_LEGAL, ids=set_ids("delivery"))
@pytest.mark.parametrize("comments", ["aaa"], ids=set_ids("comments"))
@pytest.mark.parametrize("size", SIZES, ids=set_ids("size"))
@pytest.mark.parametrize("topping", [("bacon",)], ids=set_ids("topping"))
def test_legal_one_topping(name, telephone,
                           email, delivery,
                           comments, size,
                           topping, driver):
    check_data = {
        "custname": name,
        "custtel": telephone,
        "custemail": email,
        "size": size,
        "topping": topping[0],
        "delivery": am_pm_to_24(delivery),
        "comments": comments,
    }
    driver.get(post_form_url)
    wait_for_form(driver)
    page = PizzaPage(driver)
    page.name = name
    page.telephone = telephone
    page.email = email
    page.instructions = comments
    page.set_size(size)
    page.set_delivery(delivery)
    page.set_topping(*topping)
    page.submit()
    html_body = driver.find_element_by_xpath("//body")
    response_dict = loads(html_body.text)
    form_data = response_dict.get("form")
    hamcrest.assert_that(form_data, hamcrest.has_entries(check_data))


@pytest.mark.multiple_toppings
@pytest.mark.parametrize("name", [CUSTNAME_LEGAL[0]], ids=set_ids("Name"))
@pytest.mark.parametrize("telephone", [CUSTTEL_LEGAL[0]], ids=set_ids("telephone"))
@pytest.mark.parametrize("email", [CUSTEMAIL_LEGAL[0]], ids=set_ids("email"))
@pytest.mark.parametrize("delivery", [DELIVERY_LEGAL[0]], ids=set_ids("delivery"))
@pytest.mark.parametrize("comments", ["aaa"], ids=set_ids("comments"))
@pytest.mark.parametrize("size", SIZES, ids=set_ids("size"))
@pytest.mark.parametrize("topping", [TOPPINGS], ids=set_ids("topping"))
def test_legal_mult_topping(name, telephone,
                            email, delivery,
                            comments, size,
                            topping, driver):
    topping_list = list(topping)
    check_data = {
        "custname": name,
        "custtel": telephone,
        "custemail": email,
        "size": size,
        "topping": topping_list,
        "delivery": am_pm_to_24(delivery),
        "comments": comments,
    }
    driver.get(post_form_url)
    wait_for_form(driver)
    page = PizzaPage(driver)
    page.name = name
    page.telephone = telephone
    page.email = email
    page.instructions = comments
    page.set_size(size)
    page.set_delivery(delivery)
    page.set_topping(*topping)
    page.submit()
    html_body = driver.find_element_by_xpath("//body")
    response_dict = loads(html_body.text)
    form_data = response_dict.get("form")
    hamcrest.assert_that(form_data, hamcrest.has_entries(check_data))


@pytest.mark.delivery_fail
@pytest.mark.parametrize("delivery", ILLEGAL_DELIVERY, ids=set_ids("Illegal delivery"))
def test_illegal_delivery(delivery, driver):
    driver.get(post_form_url)
    wait_for_form(driver)
    page = PizzaPage(driver, )
    page.set_delivery(delivery)
    page.submit()
    assert driver.current_url == post_form_url


@pytest.mark.email_fail
@pytest.mark.parametrize("email", ILLEGAL_EMAIL, ids=set_ids("Illegal email"))
def test_illegal_email(driver, email):
    driver.get(post_form_url)
    wait_for_form(driver)
    page = PizzaPage(driver)
    page.email = email
    page.submit()
    assert driver.current_url == post_form_url
