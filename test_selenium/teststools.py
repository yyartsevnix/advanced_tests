from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import TimeoutException
import datetime


def set_ids(name):
    def inner(value):
        return f"{name}: {value}"

    return inner


def am_pm_to_24(time_str):
    dummy_time = datetime.datetime.strptime(time_str, "%I:%M %p")
    _time = dummy_time.time()
    return _time.strftime("%H:%M")


def wait_for_form(driver):
    try:
        WebDriverWait(driver, 2).until(expected_conditions.presence_of_element_located((By.XPATH, "//form")))
    except TimeoutException:
        raise AssertionError("Post form can't be reached")
