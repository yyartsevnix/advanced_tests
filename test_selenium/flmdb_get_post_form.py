from page_objects import PageObject, PageElement
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException


class GetFormPage(PageObject):
    text = PageElement(name="text")
    number = PageElement(name="number")

    def submit(self):
        submit_button = self.w.find_element_by_xpath(xpath="//input[@name='submit']")
        submit_button.click()


class PostFormPage(GetFormPage):
    pass
