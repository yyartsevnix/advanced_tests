API_URL = "http://api.mathjs.org/v4/"

HEADER = {"User-Agent": "Python Learning Requests"}

LEGAL_VALUES = [1, 3, 1235, 0.3, -7, -835.963, 96.3, 5, -66, 1763.36, 3, -8.95436]

ILLEGAL_VALUES = ["", "kmm", "console.log(\"123\")",
                  "document.getElementById(\"1\")",
                  hex(5567), oct(3412), 168.94.hex()]

VULNERABLE_VALUES = {"Infinity": float("inf"),
                     "NaN": float("nan"),
                     "-Infinity": -float("inf")}

MEASUREMENT_UNITS = ["cm", "s"]

ALL_POSSIBLE = LEGAL_VALUES[:]

ALL_POSSIBLE.extend(VULNERABLE_VALUES.keys())
