import psycopg2
from collections import namedtuple


class Connector:
    def __init__(self, host="localhost",
                 port="5432", dbname="postgres",
                 user="postgres", password="postgres"):
        self.host = host
        self.port = port
        self.db_name = dbname
        self.user = user
        self.__password = password
        self.__connection = None
        self.cursor = None

    def __enter__(self):
        self.__connection = psycopg2.connect(dbname=self.db_name,
                                             host=self.host,
                                             port=self.port,
                                             user=self.user,
                                             password=self.__password)

        self.cursor = self.__connection.cursor()
        return self

    def __call__(self, sql_statement, classname, *args, **kwargs):

        try:
            OutClass = namedtuple(classname, args)
            self.cursor.execute(sql_statement, args)
            return [OutClass(*i) for i in self.cursor.fetchall()]
        except psycopg2.ProgrammingError as err:
            print(err)
            self.cursor.execute("rollback")

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cursor.close()
        self.__connection.close()


if __name__ == "__main__":
    with Connector(dbname="rest_countries") as conn:
        out = conn('''select \"Country\".name, 
        \"Country\".population, 
        "Capital".name, 
        "Capital".population from public."Country" 
        inner join public."Capital" 
        on "Country".capital_id="Capital".id;''',
                   "CountryAndCapitalPopulations",
                   "country_name", "country_population",
                   "capital_name", "capital_population")
        print("\n".join([str(i) for i in out]))
